alter session set "_ORACLE_SCRIPT"=true; 

--creation des rôles--
drop role pdg;
create role pdg;

drop role drh;
create role drh;

drop role directeur;
create role directeur;

drop role agent;
create role agent;

drop role responsable;
create role responsable;

drop role employe;
create role employe;

--droit à l'utilisateur de regarder les tables des deux schémas de données--
Select 'GRANT SELECT ON RLILLE.' ||Table_Name||' TO pdg;'
From All_Tables Where Owner='RLILLE';

Select 'GRANT SELECT ON RPARIS.' ||Table_Name||' TO pdg;'
From All_Tables Where Owner='RPARIS';


GRANT SELECT,INSERT,UPDATE,DELETE ON RLILLE.employe TO drh;
GRANT SELECT,INSERT,UPDATE,DELETE ON RPARIS.employe TO drh;

--droit pour regarder, insérer, modifier, supprimer dans la table centre de traitement--
GRANT SELECT,INSERT,UPDATE,DELETE ON RLILLE.centretraitement to directeur;

--droit pour regarder, insérer et modifier dans la table enreprise et tournee--
GRANT SELECT,INSERT,UPDATE ON RLILLE.entreprise to agent;
GRANT SELECT,INSERT,UPDATE ON RLILLE.tournee to agent;
--droit pour regarder la table camion et employe--
GRANT SELECT ON RLILLE.camion to agent;
GRANT SELECT ON RLILLE.employe to agent;

Select 'GRANT SELECT ON RLILLE.' ||Table_Name||' TO responsable;'
From All_Tables Where Owner='RLILLE';

--doit agir que sur sa tournee et nom les tournées du site
GRANT SELECT,INSERT,UPDATE ON RLILLE.TOURNEE to employe;
GRANT SELECT ON RLILLE.centretraitement to employe;

--profil des règles du mot de passe--
drop profile psswd_rules cascade;
create profile psswd_rules LIMIT PASSWORD_LIFE_TIME 61 failed_login_attempts 3 password_lock_time unlimited;

--création d'un utilisateur PDG NEY MARRE--
DROP USER P_NMAR;
create user P_NMAR identified BY "P_NMAR" PASSWORD EXPIRE;
--ajout des règles de mot de passe sur le user--
alter user P_NMAR profile psswd_rules;
--définit le profil PDG à l'utilisateur--
grant pdg to P_NMAR;

--création d'un utilisateur DG Krash BANDICOOM--
DROP USER D_KBAN;
create user D_KBAN identified BY "D_KBAN" PASSWORD EXPIRE;
--ajout des règles de mot de passe sur le user--
alter user D_KBAN profile psswd_rules;
--définit le profil PDG à l'utilisateur car il possède les mêmes droit que le PDG--
grant pdg to D_KBAN;

--création d'un utilisateur Directeur commercial JOHN CAFFER--
DROP USER D_JCAF;
create user D_JCAF identified BY "D_JCAF" PASSWORD EXPIRE;
--ajout des règles de mot de passe sur le user--
alter user D_JCAF profile psswd_rules;
--définit le profil PDG à l'utilisateur car il possède les mêmes droit que le PDG--
grant pdg to D_JCAF;

--création d'un directeur des ressources humaines STEVE NOTCH--
DROP USER D_SNOT;
create user D_SNOT identified BY "D_SNOT" PASSWORD EXPIRE;
--ajout des règles de mot de passe sur le user--
alter user D_SNOT profile psswd_rules;
--définit le profil drh à l'utilisateur--
grant drh to D_SNOT;

--création d'un directeur KURT KOBAIN--
DROP USER D_KKOB;
create user D_KKOB identified BY "D_KKOB" PASSWORD EXPIRE;
--ajout des règles de mot de passe sur le user--
alter user D_KKOB profile psswd_rules;
--définit le profil directeur à l'utilisateur--
grant directeur to D_KKOB;

--création d'un agent MILENN FARMER--
DROP USER A_MFAR;
create user A_MFAR identified BY "A_MFAR" PASSWORD EXPIRE;
--ajout des règles de mot de passe sur le user--
alter user A_MFAR profile psswd_rules;
--définit le profil agent à l'utilisateur--
grant agent to A_MFAR;

--création d'un responsable MARCEL PATULACCI--
DROP USER R_MPAT;
create user R_MPAT identified BY "R_MPAT" PASSWORD EXPIRE;
--ajout des règles de mot de passe sur le user--
alter user R_MPAT profile psswd_rules;
grant responsable to R_MPAT;

--création d'un employe VIN GASOIL--
DROP USER E_VGAS; 
create user E_VGAS identified BY "E_VGAS" PASSWORD EXPIRE;
--ajout des règles de mot de passe sur le user--
alter user E_VGAS profile psswd_rules;
--définit le profil employe à l'utilisateur--
grant employe to E_VGAS;
