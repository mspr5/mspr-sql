CREATE OR REPLACE FORCE NONEDITIONABLE VIEW "RRECYCL"."DEMAND_BEFORE_DATE" ("NO_DEMANDE", "DATE_DEMANDE") AS 
SELECT demande.no_demande, demande.date_demande
FROM demande
WHERE date_demande > '18/09/18';

CREATE OR REPLACE FORCE NONEDITIONABLE VIEW "RRECYCL"."DEMANDE_QUANTITE_DECHET_TOTAL" ("NO_DEMANDE", "RAISON_SOCIALE", "NO_TOURNEE", "sum_quantite", "NOM_TYPE_DECHET") AS 
SELECT demande.no_demande, entreprise.raison_sociale, tournee.no_tournee, SUM(detail_demande.quantite_enlevee) "sum_quantite", type_de_dechet.nom_type_dechet
FROM demande
INNER JOIN entreprise ON entreprise.siret = demande.entreprise_siret
INNER JOIN tournee ON tournee.id = demande.tournee_id
INNER JOIN detail_demande ON demande_id = detail_demande.demande_id
INNER JOIN type_de_dechet ON type_de_dechet.id = detail_demande.type_de_dechet_id
GROUP BY type_de_dechet.nom_type_dechet, demande.no_demande, entreprise.raison_sociale, tournee.no_tournee;

CREATE OR REPLACE FORCE NONEDITIONABLE VIEW "RRECYCL"."DEMANDE_SANS_TOURNEES" ("ID", "NO_DEMANDE", "TOURNEE_ID") AS 
SELECT id, no_demande, tournee_id FROM demande WHERE tournee_id IS NULL;

CREATE OR REPLACE FORCE NONEDITIONABLE VIEW "RRECYCL"."EMPLOYE_NB_TOURNEE" ("NOM", "PRENOM", "DATE_NAISSANCE", "nombre_tournee") AS 
SELECT nom, prenom, date_naissance, COUNT(tournee.id) "nombre_tournee"
FROM employe
INNER JOIN tournee ON tournee.employe_id = employe.id
GROUP BY nom, prenom, date_naissance
HAVING COUNT(tournee.id) < 4;

CREATE OR REPLACE FORCE NONEDITIONABLE VIEW "RRECYCL"."ENTREPRISE_NB_DEMANDE_SUPERIEURE_TO" ("SIRET", "RAISON_SOCIALE", "NbDemande") AS 
SELECT entreprise.siret, entreprise.raison_sociale, SUM(demande.id) "NbDemande"
FROM entreprise
INNER JOIN demande
ON entreprise.siret = demande.entreprise_siret
GROUP BY entreprise.siret, entreprise.raison_sociale
HAVING SUM(demande.id) > (SELECT SUM(demande.id)"NbDemandeCompare" FROM demande
INNER JOIN entreprise ON entreprise.siret = demande.entreprise_siret
WHERE entreprise.raison_sociale LIKE 'Formalys');

CREATE OR REPLACE FORCE NONEDITIONABLE VIEW "RRECYCL"."QUANTITE_TYPE_DECHET_PAR_DATE" ("DATE_ENLEVEMENT", "sum_quantite", "NOM_TYPE_DECHET") AS 
SELECT demande.date_enlevement, SUM(detail_demande.quantite_enlevee) "sum_quantite", type_de_dechet.nom_type_dechet
FROM detail_demande
INNER JOIN type_de_dechet ON detail_demande.type_de_dechet_id = type_de_dechet.id
INNER JOIN demande ON demande.id = detail_demande.demande_id
WHERE demande.date_enlevement = '30/09/18'
GROUP BY type_de_dechet.nom_type_dechet, demande.date_enlevement;