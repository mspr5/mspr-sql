create or replace NONEDITIONABLE PROCEDURE QUANTITE_COLLECTE_TYPE_DECHET_SITE
(
  SITE_NOM IN VARCHAR2 
, PERIODE IN VARCHAR2
, TYPE_DECHET IN VARCHAR2 
, SITE_NOM_SORTIE OUT VARCHAR2 
, PERIODE_SORTIE OUT VARCHAR2
, TYPE_DECHET_SORTIE OUT VARCHAR2 
, QUANTITE_SORTIE OUT NUMBER 
) AS 
BEGIN
  SELECT SUM(detail_demande.quantite_enlevee) "quantite_totale", demande.date_enlevement, site.nom_site ,type_de_dechet.nom_type_dechet
    INTO quantite_sortie, site_nom_sortie, periode_sortie,type_dechet_sortie
    FROM detail_demande
    INNER JOIN demande ON demande.id = detail_demande.demande_id
    INNER JOIN type_de_dechet ON type_de_dechet.id = detail_demande.type_de_dechet_id
    INNER JOIN site ON demande.site_id = site.id
    group by site.nom_site, demande.date_enlevement, type_de_dechet.nom_type_dechet
    HAVING site.nom_site = SITE_NOM AND type_de_dechet.nom_type_dechet = TYPE_DECHET AND demande.date_enlevement = periode;
    
    DBMS_OUTPUT.put_line(quantite_sortie || ' ' || site_nom_sortie || ' ' || periode_sortie || ' ' || type_dechet_sortie);
END QUANTITE_COLLECTE_TYPE_DECHET_SITE;


CREATE OR REPLACE PROCEDURE QUANTITE_COLLECTE_TYPE_DECHET_NATIONAL 
(
 PERIODE IN VARCHAR2
, TYPE_DECHET IN VARCHAR2 
, SITE_NOM_SORTIE OUT VARCHAR2 
, PERIODE_SORTIE OUT VARCHAR2
, TYPE_DECHET_SORTIE OUT VARCHAR2 
, QUANTITE_SORTIE OUT NUMBER 
) AS 
BEGIN
  SELECT SUM(detail_demande.quantite_enlevee) "quantite_totale", demande.date_enlevement, site.nom_site ,type_de_dechet.nom_type_dechet
    INTO quantite_sortie, site_nom_sortie, periode_sortie,type_dechet_sortie
    FROM detail_demande
    INNER JOIN demande ON demande.id = detail_demande.demande_id
    INNER JOIN type_de_dechet ON type_de_dechet.id = detail_demande.type_de_dechet_id
    INNER JOIN site ON demande.site_id = site.id
    group by site.nom_site, demande.date_enlevement, type_de_dechet.nom_type_dechet
    HAVING type_de_dechet.nom_type_dechet = TYPE_DECHET AND demande.date_enlevement = periode;
    
    DBMS_OUTPUT.put_line(quantite_sortie || ' ' || site_nom_sortie || ' ' || periode_sortie || ' ' || type_dechet_sortie);
END QUANTITE_COLLECTE_TYPE_DECHET_NATIONAL;

CREATE OR REPLACE TRIGGER verification_quantite_dechet BEFORE INSERT ON detail_depot
FOR EACH ROW
DECLARE
    quantite_remise INT;
	quantite_prise INT;
	no_tournee INT ;
	type_dechet INT;
BEGIN
    no_tournee := :new.tournee_id;
	type_dechet := :new.type_de_dechet_id;
	quantite_remise := :new.quantite_deposee;
	SELECT detail_demande.quantite_enlevee INTO quantite_prise FROM detail_demande
	INNER JOIN demande ON demande.id = detail_demande.demande_id
	INNER JOIN tournee ON tournee.id = demande.tournee_id
    INNER JOIN type_de_dechet ON type_de_dechet.id = detail_demande.type_de_dechet_id
	WHERE tournee.id = no_tournee AND type_de_dechet.id = type_dechet;

	IF quantite_remise <= quantite_prise THEN
        INSERT INTO detail_depot(quantite_deposee, type_de_dechet_id, tournee_id, centre_traitement_id)
        VALUES (:new.quantite_deposee, :new.type_de_dechet_id, :new.tournee_id, :new.centre_traitement_id);
    END IF;
END;
