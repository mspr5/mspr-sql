GRANT UNLIMITED TABLESPACE TO RRECYCL;

DEFINE PARIS_SITE_ID = 1;
DEFINE LILLE_SITE_ID = 2;
DEFINE LYON_SITE_ID = 3;
DEFINE NANTES_SITE_ID = 4;

-- Site
INSERT INTO RRECYCL.SITE (ID, NOM_SITE, ADRESSE)
VALUES (&PARIS_SITE_ID, 'Paris', '20 bis Jardins Boieldieu 92071 La Défense');
INSERT INTO RRECYCL.SITE (ID, NOM_SITE, ADRESSE)
VALUES (&LILLE_SITE_ID, 'Lille', '91 rue Nationale 59000 Lille');
INSERT INTO RRECYCL.SITE (ID, NOM_SITE, ADRESSE)
VALUES (&LYON_SITE_ID, 'Lyon', '16, boulevard Général de Gaulle 44200 Nantes');
INSERT INTO RRECYCL.SITE (ID, NOM_SITE, ADRESSE)
VALUES (&NANTES_SITE_ID, 'Nantes', '7 rue Jean-Marie LECLAIR 69009 Lyon');

-- Entreprise
INSERT INTO RRECYCL.ENTREPRISE (SIRET, RAISON_SOCIALE, NO_RUE_ENTR, RUE_ENTR, CP_ENTR, VILLE_ENTR, NO_TEL, CONTACT)
SELECT SIRET,
       RAISONSOCIALE,
       NORUEENTR,
       RUEENTR,
       CPOSTALENTR,
       VILLEENTR,
       NOTEL,
       CONTACT
FROM RLILLE.ENTREPRISE;

INSERT INTO RRECYCL.ENTREPRISE (SIRET, RAISON_SOCIALE, NO_RUE_ENTR, RUE_ENTR, CP_ENTR, VILLE_ENTR, NO_TEL, CONTACT)
SELECT SIRET,
       RAISONSOCIALE,
       NORUEENTR,
       RUEENTR,
       CPOSTALENTR,
       VILLEENTR,
       NOTEL,
       CONTACT
FROM RPARIS.ENTREPRISE;

-- Centre traitement
INSERT INTO RRECYCL.CENTRE_TRAITEMENT (NO_CENTRE, NOM_CENTRE, NO_RUE_CENTRE, RUE_CENTRE, CP_CENTRE, VILLE_CENTRE, SITE_ID)
SELECT NOCENTRE, NOMCENTRE, NORUECENTRE, RUECENTRE, CPOSTALCENTRE, VILLECENTRE, &LILLE_SITE_ID
FROM RLILLE.CENTRETRAITEMENT;

INSERT INTO RRECYCL.CENTRE_TRAITEMENT (NO_CENTRE, NOM_CENTRE, NO_RUE_CENTRE, RUE_CENTRE, CP_CENTRE, VILLE_CENTRE, SITE_ID)
SELECT NOCENTRE, NOMCENTRE, NORUECENTRE, RUECENTRE, CPOSTALCENTRE, VILLECENTRE, &PARIS_SITE_ID
FROM RPARIS.CENTRETRAITEMENT;

-- Fonction
INSERT INTO RRECYCL.FONCTION (NOM_FONCTION)
SELECT NOMFONCTION
FROM RLILLE.FONCTION;

-- Type de déchet
-- Les deux tables sont identiques
INSERT INTO RRECYCL.TYPE_DE_DECHET (NOM_TYPE_DECHET, NIV_DECHET)
SELECT NOMTYPEDECHET, NIV_DANGER
FROM RLILLE.TYPEDECHET;

-- Camion
INSERT INTO RRECYCL.CAMION (IMMATRICULATION, DATE_ACHAT, MODELE, MARQUE, SITE_ID)
SELECT NOIMMATRIC, DATEACHAT, MODELE, MARQUE, &LILLE_SITE_ID
FROM RLILLE.CAMION;

INSERT INTO RRECYCL.CAMION (IMMATRICULATION, DATE_ACHAT, MODELE, MARQUE, SITE_ID)
SELECT NOIMMATRIC, DATEACHAT, MODELE, MARQUE, &PARIS_SITE_ID
FROM RPARIS.CAMION;

-- Employé
INSERT INTO RRECYCL.EMPLOYE (NO_EMPLOYE, NOM, PRENOM, DATE_NAISSANCE, DATE_EMBAUCHE, SALAIRE, FONCTION_ID, SITE_ID)
SELECT NOEMPLOYE,
       NOM,
       PRENOM,
       DATENAISS,
       DATEEMBAUCHE,
       SALAIRE,
       NOFONCTION,
       &LILLE_SITE_ID
FROM RLILLE.EMPLOYE;

CREATE OR REPLACE PROCEDURE rparis_employe IS
    c_noemploye             RPARIS.EMPLOYE.NOEMPLOYE%TYPE;
    c_nom                   RPARIS.EMPLOYE.NOM%TYPE;
    c_prenom                RPARIS.EMPLOYE.PRENOM%TYPE;
    c_datenaiss             RPARIS.EMPLOYE.DATENAISS%TYPE;
    c_dateembauche          RPARIS.EMPLOYE.DATEEMBAUCHE%TYPE;
    c_salaire               RPARIS.EMPLOYE.SALAIRE%TYPE;
    c_fonction              RPARIS.EMPLOYE.FONCTION%TYPE;
    c_fonction_id           RRECYCL.FONCTION.ID%TYPE;
    CURSOR c_employe is
        SELECT NOEMPLOYE, NOM, PRENOM, DATENAISS, DATEEMBAUCHE, SALAIRE, FONCTION
        FROM RPARIS.EMPLOYE;
BEGIN
    OPEN c_employe;
    LOOP
        FETCH c_employe into c_noemploye, c_nom, c_prenom, c_datenaiss, c_dateembauche, c_salaire, c_fonction;
        EXIT WHEN c_employe%notfound;
        -- on récupère id de la fonction de l’employé
        SELECT ID INTO c_fonction_id FROM RRECYCL.FONCTION WHERE NOM_FONCTION = c_fonction;
        -- on insére un enregistrement dans la table EMPLOYE
        INSERT INTO RRECYCL.EMPLOYE (NO_EMPLOYE, NOM, PRENOM, DATE_NAISSANCE, DATE_EMBAUCHE, SALAIRE, FONCTION_ID, SITE_ID)
        VALUES (c_noemploye, c_nom, c_prenom, c_datenaiss, c_dateembauche, c_salaire, c_fonction_id, &PARIS_SITE_ID);
    END LOOP;
    CLOSE c_employe;
END;
/

EXECUTE rparis_employe;

-- Tournée
CREATE OR REPLACE PROCEDURE rparis_tournee IS
    c_notournee   RPARIS.TOURNEE.NOTOURNEE%TYPE;
    c_datetournee RPARIS.TOURNEE.DATETOURNEE%TYPE;
    c_noimmatric  RPARIS.TOURNEE.NOIMMATRIC%TYPE;
    c_noemploye   RPARIS.TOURNEE.NOEMPLOYE%TYPE;
    c_employe_id  RRECYCL.EMPLOYE.ID%TYPE;
    CURSOR c_tournee is
        SELECT NOTOURNEE, DATETOURNEE, NOIMMATRIC, NOEMPLOYE
        FROM RPARIS.TOURNEE;
BEGIN
    OPEN c_tournee;
    LOOP
        FETCH c_tournee into c_notournee, c_datetournee, c_noimmatric, c_noemploye;
        EXIT WHEN c_tournee%notfound;
        -- on récupère id de l’employé
        SELECT ID INTO c_employe_id FROM RRECYCL.EMPLOYE WHERE NO_EMPLOYE = c_noemploye AND SITE_ID = &PARIS_SITE_ID;
        -- on insére un enregistrement dans la table TOURNEE
        INSERT INTO RRECYCL.TOURNEE (NO_TOURNEE, DATE_TOURNEE, CAMION_IMMATRICULATION, EMPLOYE_ID, SITE_ID)
        VALUES (c_notournee, c_datetournee, c_noimmatric, c_employe_id, &PARIS_SITE_ID);
    END LOOP;
    CLOSE c_tournee;
END;
/

EXECUTE rparis_tournee;

CREATE OR REPLACE PROCEDURE rlille_tournee IS
    c_notournee   RLILLE.TOURNEE.NOTOURNEE%TYPE;
    c_datetournee RLILLE.TOURNEE.DATETOURNEE%TYPE;
    c_noimmatric  RLILLE.TOURNEE.NOIMMATRIC%TYPE;
    c_noemploye   RLILLE.TOURNEE.NOEMPLOYE%TYPE;
    c_employe_id  RRECYCL.EMPLOYE.ID%TYPE;
    CURSOR c_tournee is SELECT NOTOURNEE, DATETOURNEE, NOIMMATRIC, NOEMPLOYE
                        FROM RLILLE.TOURNEE;
BEGIN
    OPEN c_tournee;
    LOOP
        FETCH c_tournee into c_notournee, c_datetournee, c_noimmatric, c_noemploye;
        EXIT WHEN c_tournee%notfound;
        -- on récupère id de l’employé
        SELECT ID INTO c_employe_id FROM RRECYCL.EMPLOYE WHERE NO_EMPLOYE = c_noemploye AND SITE_ID = &LILLE_SITE_ID;
        -- on insére un enregistrement dans la table TOURNEE
        INSERT INTO RRECYCL.TOURNEE (NO_TOURNEE, DATE_TOURNEE, CAMION_IMMATRICULATION, EMPLOYE_ID, SITE_ID)
        VALUES (c_notournee, c_datetournee, c_noimmatric, c_employe_id, &LILLE_SITE_ID);
    END LOOP;
    CLOSE c_tournee;
END;
/

EXECUTE rlille_tournee;

-- Demande
CREATE OR REPLACE PROCEDURE rparis_demande IS
    c_nodemande      RPARIS.DEMANDE.NODEMANDE%TYPE;
    c_datedemande    RPARIS.DEMANDE.DATEDEMANDE%TYPE;
    c_dateenlevement RPARIS.DEMANDE.DATEENLEVEMENT%TYPE;
    c_web_o_n        RPARIS.DEMANDE.WEB_O_N%TYPE;
    c_siret          RPARIS.DEMANDE.SIRET%TYPE;
    c_notournee      RPARIS.DEMANDE.NOTOURNEE%TYPE;
    c_tournee_id     RRECYCL.TOURNEE.ID%TYPE;
    c_web            RRECYCL.DEMANDE.WEB%TYPE;
    CURSOR c_demande is
        SELECT NODEMANDE, DATEDEMANDE, DATEENLEVEMENT, WEB_O_N, SIRET, NOTOURNEE
        FROM RPARIS.DEMANDE;
BEGIN
    OPEN c_demande;
    LOOP
        FETCH c_demande into c_nodemande, c_datedemande, c_dateenlevement, c_web_o_n, c_siret, c_notournee;
        EXIT WHEN c_demande%notfound;
        -- on récupère id de la tournée
        IF c_notournee IS NOT NULL THEN
            SELECT ID INTO c_tournee_id FROM RRECYCL.TOURNEE WHERE NO_TOURNEE = c_notournee AND SITE_ID = &PARIS_SITE_ID;
        ELSE
            c_tournee_id := NULL;
        END IF;
        -- on transforme le O|N en 1|0
        IF c_web_o_n = 'O' THEN
            c_web := 1;
        ELSE
            c_web := 0;
        END IF;
        -- on insére un enregistrement dans la table DEMANDE
        INSERT INTO RRECYCL.DEMANDE (NO_DEMANDE, DATE_DEMANDE, DATE_ENLEVEMENT, WEB, SITE_ID, ENTREPRISE_SIRET, TOURNEE_ID)
        VALUES (c_nodemande, c_datedemande, c_dateenlevement, c_web, &PARIS_SITE_ID, c_siret, c_tournee_id);
    END LOOP;
    CLOSE c_demande;
END;
/

EXECUTE rparis_demande;

CREATE OR REPLACE PROCEDURE rlille_demande IS
    c_nodemande      RLILLE.DEMANDE.NODEMANDE%TYPE;
    c_datedemande    RLILLE.DEMANDE.DATEDEMANDE%TYPE;
    c_dateenlevement RLILLE.DEMANDE.DATEENLEVEMENT%TYPE;
    c_siret          RLILLE.DEMANDE.SIRET%TYPE;
    c_notournee      RLILLE.DEMANDE.NOTOURNEE%TYPE;
    c_tournee_id     RRECYCL.TOURNEE.ID%TYPE;
    CURSOR c_demande is
        SELECT NODEMANDE, DATEDEMANDE, DATEENLEVEMENT, SIRET, NOTOURNEE
        FROM RLILLE.DEMANDE;
BEGIN
    OPEN c_demande;
    LOOP
        FETCH c_demande into c_nodemande, c_datedemande, c_dateenlevement, c_siret, c_notournee;
        EXIT WHEN c_demande%notfound;
        -- on récupère id de la tournée
        IF c_notournee IS NOT NULL THEN
            SELECT ID INTO c_tournee_id FROM RRECYCL.TOURNEE WHERE NO_TOURNEE = c_notournee AND SITE_ID = &LILLE_SITE_ID;
        ELSE
            c_tournee_id := NULL;
        END IF;
        -- on insére un enregistrement dans la table DEMANDE
        INSERT INTO RRECYCL.DEMANDE (NO_DEMANDE, DATE_DEMANDE, DATE_ENLEVEMENT, WEB, SITE_ID, ENTREPRISE_SIRET, TOURNEE_ID)
        VALUES (c_nodemande, c_datedemande, c_dateenlevement, NULL, &LILLE_SITE_ID, c_siret, c_tournee_id);
    END LOOP;
    CLOSE c_demande;
END;
/

EXECUTE rlille_demande;

-- Détail demande
CREATE OR REPLACE PROCEDURE rparis_detail_demande IS
    c_quantiteenlevee   RPARIS.DETAILDEMANDE.QUANTITEENLEVEE%TYPE;
    c_nodemande         RPARIS.DETAILDEMANDE.NODEMANDE%TYPE;
    c_notypedechet      RPARIS.DETAILDEMANDE.NOTYPEDECHET%TYPE;
    c_demande_id        RRECYCL.DEMANDE.ID%TYPE;
    CURSOR c_detaildemande is
        SELECT QUANTITEENLEVEE, NODEMANDE, NOTYPEDECHET
        FROM RPARIS.DETAILDEMANDE;
BEGIN
    OPEN c_detaildemande;
    LOOP
        FETCH c_detaildemande into c_quantiteenlevee, c_nodemande, c_notypedechet;
        EXIT WHEN c_detaildemande%notfound;
        -- on récupère id de la demande
        SELECT ID INTO c_demande_id FROM RRECYCL.DEMANDE WHERE NO_DEMANDE = c_nodemande AND SITE_ID = &PARIS_SITE_ID;
        -- on insére un enregistrement dans la table DETAIL_DEMANDE
        INSERT INTO RRECYCL.DETAIL_DEMANDE (QUANTITE_ENLEVEE, DEMANDE_ID, TYPE_DE_DECHET_ID)
        VALUES (c_quantiteenlevee, c_demande_id, c_notypedechet);
    END LOOP;
    CLOSE c_detaildemande;
END;
/

EXECUTE rparis_detail_demande;

CREATE OR REPLACE PROCEDURE rlille_detail_demande IS
    c_quantiteenlevee   RLILLE.DETAILDEMANDE.QUANTITEENLEVEE%TYPE;
    c_nodemande         RLILLE.DETAILDEMANDE.NODEMANDE%TYPE;
    c_notypedechet      RLILLE.DETAILDEMANDE.NOTYPEDECHET%TYPE;
    c_demande_id        RRECYCL.DEMANDE.ID%TYPE;
    CURSOR c_detaildemande is
        SELECT QUANTITEENLEVEE, NODEMANDE, NOTYPEDECHET
        FROM RLILLE.DETAILDEMANDE;
BEGIN
    OPEN c_detaildemande;
    LOOP
        FETCH c_detaildemande into c_quantiteenlevee, c_nodemande, c_notypedechet;
        EXIT WHEN c_detaildemande%notfound;
        -- on récupère id de la demande
        SELECT ID INTO c_demande_id FROM RRECYCL.DEMANDE WHERE NO_DEMANDE = c_nodemande AND SITE_ID = &LILLE_SITE_ID;
        -- on insére un enregistrement dans la table DETAIL_DEMANDE
        INSERT INTO RRECYCL.DETAIL_DEMANDE (QUANTITE_ENLEVEE, DEMANDE_ID, TYPE_DE_DECHET_ID)
        VALUES (c_quantiteenlevee, c_demande_id, c_notypedechet);
    END LOOP;
    CLOSE c_detaildemande;
END;
/

EXECUTE rlille_detail_demande;

-- Détail dépot
CREATE OR REPLACE PROCEDURE rparis_detail_depot IS
    c_quantitedeposee      RPARIS.DETAILDEPOT.QUANTITEDEPOSEE%TYPE;
    c_notournee            RPARIS.DETAILDEPOT.NOTOURNEE%TYPE;
    c_notypedechet         RPARIS.DETAILDEPOT.NOTYPEDECHET%TYPE;
    c_nocentre             RPARIS.DETAILDEPOT.NOCENTRE%TYPE;
    c_tournee_id           RRECYCL.TOURNEE.ID%TYPE;
    c_centre_traitement_id RRECYCL.CENTRE_TRAITEMENT.ID%TYPE;
    CURSOR c_detaildepot is
        SELECT QUANTITEDEPOSEE, NOTOURNEE, NOTYPEDECHET, NOCENTRE
        FROM RPARIS.DETAILDEPOT;
BEGIN
    OPEN c_detaildepot;
    LOOP
        FETCH c_detaildepot into c_quantitedeposee, c_notournee, c_notypedechet, c_nocentre;
        EXIT WHEN c_detaildepot%notfound;
        -- on récupère id de la tournée
        SELECT ID INTO c_tournee_id FROM RRECYCL.TOURNEE WHERE NO_TOURNEE = c_notournee AND SITE_ID = &PARIS_SITE_ID;
        -- on récupère id du centre de traitement
        SELECT ID INTO c_centre_traitement_id FROM RRECYCL.CENTRE_TRAITEMENT WHERE NO_CENTRE = c_nocentre AND SITE_ID = &PARIS_SITE_ID;
        -- on insére un enregistrement dans la table DETAIL_DEPOT
        INSERT INTO RRECYCL.DETAIL_DEPOT (QUANTITE_DEPOSEE, TYPE_DE_DECHET_ID, TOURNEE_ID, CENTRE_TRAITEMENT_ID)
        VALUES (c_quantitedeposee, c_notypedechet, c_tournee_id, c_centre_traitement_id);
    END LOOP;
    CLOSE c_detaildepot;
END;
/

EXECUTE rparis_detail_depot;

CREATE OR REPLACE PROCEDURE rlille_detail_depot IS
    c_quantitedeposee      RLILLE.DETAILDEPOT.QUANTITEDEPOSEE%TYPE;
    c_notournee            RLILLE.DETAILDEPOT.NOTOURNEE%TYPE;
    c_notypedechet         RLILLE.DETAILDEPOT.NOTYPEDECHET%TYPE;
    c_nocentre             RLILLE.DETAILDEPOT.NOCENTRE%TYPE;
    c_tournee_id           RRECYCL.TOURNEE.ID%TYPE;
    c_centre_traitement_id RRECYCL.CENTRE_TRAITEMENT.ID%TYPE;
    CURSOR c_detaildepot is
        SELECT QUANTITEDEPOSEE, NOTOURNEE, NOTYPEDECHET, NOCENTRE
        FROM RLILLE.DETAILDEPOT;
BEGIN
    OPEN c_detaildepot;
    LOOP
        FETCH c_detaildepot into c_quantitedeposee, c_notournee, c_notypedechet, c_nocentre;
        EXIT WHEN c_detaildepot%notfound;
        -- on récupère id de la tournée
        SELECT ID INTO c_tournee_id FROM RRECYCL.TOURNEE WHERE NO_TOURNEE = c_notournee AND SITE_ID = &LILLE_SITE_ID;
        -- on récupère id du centre de traitement
        SELECT ID INTO c_centre_traitement_id FROM RRECYCL.CENTRE_TRAITEMENT WHERE NO_CENTRE = c_nocentre AND SITE_ID = &LILLE_SITE_ID;
        -- on insére un enregistrement dans la table DETAIL_DEPOT
        INSERT INTO RRECYCL.DETAIL_DEPOT (QUANTITE_DEPOSEE, TYPE_DE_DECHET_ID, TOURNEE_ID, CENTRE_TRAITEMENT_ID)
        VALUES (c_quantitedeposee, c_notypedechet, c_tournee_id, c_centre_traitement_id);
    END LOOP;
    CLOSE c_detaildepot;
END;
/

EXECUTE rlille_detail_depot;


ALTER TABLE RRECYCL.TOURNEE DROP COLUMN site_id;
ALTER TABLE RRECYCL.CENTRE_TRAITEMENT DROP COLUMN site_id;